import { Component, OnInit } from '@angular/core';
import { IUser } from '../user';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: IUser 
  = {
    username: '',
    firstname: '',
    lastname: '',
    email: '',
    password: '',
    dob: '',
    addressline1: '',
    addressline2: '',
    city: '',
    state: '',
    country: '',
    zipcode: ''
   
  };
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    const {username, firstname, lastname, email, password, dob, addressline1, addressline2, city, state,country,
       zipcode } = this.form;

  
  }
}