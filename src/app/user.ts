export interface IUser
{
    username: string,
    firstname: string,
    lastname: string,
    email: string,
    password: string,
    dob: string,
    addressline1: string,
    addressline2: string,
    city: string,
    state: string,
    country: string,
    zipcode: string
  
}